namespace Slack.Domain.Implementations.Picsou.Tests.Helpers
{
    using Slack.Domain.Contracts.Commands;

    using TechTalk.SpecFlow;

    /// <summary> The properties helper. </summary>
    public class PropertiesHelper
    {
        /// <summary> Gets or sets the command. </summary>
        public static IMessageCommand Command
        {
            get
            {
                return (IMessageCommand)ScenarioContext.Current["Command"];
            }
            set
            {
                ScenarioContext.Current.Add("Command", value);
            }
        }

        /// <summary> Gets or sets the message. </summary>
        public static string Message
        {
            get
            {
                return (string)ScenarioContext.Current["Message"];
            }
            set
            {
                ScenarioContext.Current.Add("Message", value);
            }
        }
    }
}