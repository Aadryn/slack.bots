﻿@Slack @Bot @Picsou @Command @Help
Feature: Command - Help
	As slack user
	I want to be able to ask for help
	In order to retrieve the list of available command

Scenario: The command should be able to execute help when the received message "Picsou: Can you help me"
	Given a new HelpCommand
	  And the message "Picsou: Can you help me"
	 When i invoke the method CanExecute of the command
	 Then the result of the invocation of the CanExecute method should be true

Scenario: The command should be able to execute help when the received message "Picsou: Help"
	Given a new HelpCommand
	  And the message "Picsou: Help"
	 When i invoke the method CanExecute of the command
	 Then the result of the invocation of the CanExecute method should be true