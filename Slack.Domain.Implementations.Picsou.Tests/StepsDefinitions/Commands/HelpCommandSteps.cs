﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HelpCommandSteps.cs" company="">
//   
// </copyright>
// <summary>
//   The help command steps.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Slack.Domain.Implementations.Picsou.Tests.StepsDefinitions.Commands
{
    using Slack.Domain.Implementations.Picsou.Commands.Requests;
    using Slack.Domain.Implementations.Picsou.Tests.Helpers;

    using TechTalk.SpecFlow;

    /// <summary> The help command steps. </summary>
    [Binding]
    public class HelpCommandSteps
    {
        /// <summary> The given a new help command. </summary>
        [Given(@"a new HelpCommand")]
        public void GivenANewHelpCommand()
        {
            PropertiesHelper.Command = new HelpCommand();
        }
    }
}
