﻿namespace Slack.Domain.Implementations.Picsou.Tests.StepsDefinitions.Commands
{
    using NFluent;

    using Slack.Domain.Implementations.Picsou.Tests.Helpers;

    using TechTalk.SpecFlow;

    /// <summary> The shared command steps. </summary>
    [Binding]
    public class SharedCommandSteps
    {
        /// <summary> Gets or sets the message. </summary>
        private string Message
        {
            get;
            set;
        }

        /// <summary> Gets or sets a value indicating whether can execute result. </summary>
        private bool CanExecuteResult
        {
            get;
            set;
        }

        /// <summary> Step: Given the message. </summary>
        /// <param name="message"> The message. </param>
        [Given(@"the message ""(.*)""")]
        public void GivenTheMessage(string message)
        {
            this.Message = message;
        }

        /// <summary> Step: When i invoke the method can execute of the command. </summary>
        [When(@"i invoke the method CanExecute of the command")]
        public void WhenIInvokeTheMethodCanExecuteOfTheCommand()
        {
            this.CanExecuteResult = PropertiesHelper.Command.CanExecute(this.Message);
        }

        /// <summary> Step: Then the result of the invocation of the can execute method should be true. </summary>
        [Then(@"the result of the invocation of the CanExecute method should be true")]
        public void ThenTheResultOfTheInvocationOfTheCanExecuteMethodShouldBeTrue()
        {
            Check.That(this.CanExecuteResult).IsTrue();
        }
    }
}
