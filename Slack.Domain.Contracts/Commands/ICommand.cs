﻿namespace Slack.Domain.Contracts.Commands
{
    /// <summary> The Command interface. </summary>
    public interface ICommand
    {
        /// <summary> The execute. </summary>
        /// <returns> The <see cref="string"/>. </returns>
        string Execute();
    }
}
