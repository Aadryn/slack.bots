﻿namespace Slack.Domain.Contracts.Commands
{
    /// <summary> The Command interface. </summary>
    public interface IMessageCommand
    {
        /// <summary> The can execute. </summary>
        /// <param name="message"> The message. </param>
        /// <returns> The <see cref="bool"/>. </returns>
        bool CanExecute(string message);

        /// <summary> The execute. </summary>
        /// <returns> The <see cref="string"/>. </returns>
        string Execute();
    }
}
