﻿namespace Slack.Domain.Contracts
{
    /// <summary> The Bot interface. </summary>
    public interface IBot
    {
        /// <summary> The run. </summary>
        void Run();
    }
}
