namespace Slack.Domain.Models
{
    using Newtonsoft.Json;

    /// <summary> The login response message. </summary>
    public class LoginResponseMessage
    {
        [JsonProperty("url")]
        public string Url
        {
            get;
            set;
        }
    }
}