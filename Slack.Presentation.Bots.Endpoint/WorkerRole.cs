namespace Slack.Presentation.Bots.Endpoint
{
    using System.Diagnostics;
    using System.Net;
    using System.Threading;
    using System.Threading.Tasks;

    using Microsoft.WindowsAzure.ServiceRuntime;

    /// <summary> The worker role. </summary>
    public class WorkerRole : RoleEntryPoint
    {
        /// <summary> The cancellation token source. </summary>
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        /// <summary> The run complete event. </summary>
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);

        /// <summary> The run. </summary>
        public override void Run()
        {
            Trace.TraceInformation("Slack.Presentation.Bots.Endpoint is running");

            try
            {
                this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        /// <summary> The on start. </summary>
        /// <returns> The <see cref="bool"/>. </returns>
        public override bool OnStart()
        {
            ServicePointManager.DefaultConnectionLimit = 12;

            var result = base.OnStart();

            Trace.TraceInformation("Slack.Presentation.Bots.Endpoint has been started");

            return result;
        }

        /// <summary> The on stop. </summary>
        public override void OnStop()
        {
            Trace.TraceInformation("Slack.Presentation.Bots.Endpoint is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("Slack.Presentation.Bots.Endpoint has stopped");
        }

        /// <summary> The run async. </summary>
        /// <param name="cancellationToken"> The cancellation token. </param>
        /// <returns> The <see cref="Task"/>. </returns>
        private async Task RunAsync(CancellationToken cancellationToken)
        {
            // TODO: Replace the following with your own logic.
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("Working");
                await Task.Delay(1000);
            }
        }
    }
}
