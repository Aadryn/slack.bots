﻿namespace Slack.Domain.Implementations.Picsou
{
    using System.Collections.Generic;

    using Slack.Domain.Contracts.Commands;

    /// <summary> The picsou bot. </summary>
    public class PicsouBot : BaseBot
    {
        /// <summary> Initializes a new instance of the <see cref="PicsouBot"/> class.  Initializes a new instance of the <see cref="BaseBot"/> class. </summary>
        /// <param name="token"> The token. </param>
        /// <param name="commands"> The commands. </param>
        /// <param name="messageCommands"> The message Commands. </param>
        public PicsouBot(string token, IEnumerable<ICommand> commands, IEnumerable<IMessageCommand> messageCommands)
            : base(token, commands, messageCommands)
        {
        }
    }
}
