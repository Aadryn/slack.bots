﻿namespace Slack.Domain.Implementations.Picsou.Commands.Requests
{
    using Slack.Domain.Contracts.Commands;

    /// <summary> The list debts command. </summary>
    public class ListDebtsCommand : IMessageCommand
    {
        /// <summary> The can execute. </summary>
        /// <param name="message"> The message. </param>
        /// <returns> The <see cref="bool"/>. </returns>
        public bool CanExecute(string message)
        {
            throw new System.NotImplementedException();
        }

        /// <summary> The execute. </summary>
        /// <returns> The <see cref="string"/>. </returns>
        public string Execute()
        {
            throw new System.NotImplementedException();
        }
    }
}
