﻿namespace Slack.Domain.Implementations.Picsou.Commands.Requests
{
    using System.Collections.Generic;
    using System.Linq;

    using Slack.Domain.Contracts.Commands;

    /// <summary> The help command. </summary>
    public class HelpCommand : IMessageCommand
    {
        /// <summary> Initializes a new instance of the <see cref="HelpCommand"/> class. </summary>
        public HelpCommand()
        {
            this.Commands = new List<string>()
                                {
                                    "Can you help me", 
                                    "Help"
                                };
        }

        /// <summary> Gets or sets the commands. </summary>
        private IEnumerable<string> Commands
        {
            get;
            set;
        }

        /// <summary> Gets or sets the message. </summary>
        private string Message
        {
            get;
            set;
        }

        /// <summary> The can execute. </summary>
        /// <param name="message"> The message. </param>
        /// <returns> The <see cref="bool"/>. </returns>
        public bool CanExecute(string message)
        {
            this.Message = message;
            return this.CheckThatMessageStartWithPicsou(message) 
                && this.CheckThatCommandExistInAuthorized(message);
        }

        /// <summary> The execute. </summary>
        /// <returns> The message to return in response <see cref="string"/>. </returns>
        public string Execute()
        {
            throw new System.NotImplementedException();
        }

        /// <summary> Checks that command exist in authorized. </summary>
        /// <param name="message"> The message to check. </param>
        /// <returns> The value of the check <see cref="bool"/>. </returns>
        private bool CheckThatCommandExistInAuthorized(string message)
        {
            var commands = this.Commands.Select(command => command.ToLower());
            var splitResult = message.Split(':');
            return splitResult.Count() > 1 && commands.Contains(splitResult[1].Trim().ToLower());
        }

        /// <summary> Checks that message start with picsou. </summary>
        /// <param name="message"> The message to check. </param>
        /// <returns> The value of the check <see cref="bool"/>. </returns>
        private bool CheckThatMessageStartWithPicsou(string message)
        {
            return message.StartsWith("Picsou:");
        }
    }
}
