﻿namespace Slack.Domain.Implementations.Picsou.Commands.Messages
{
    using System;

    using Slack.Domain.Contracts.Commands;

    /// <summary> The say goodby command. </summary>
    public class SayGoodbyeCommand : ICommand
    {
        /// <summary> The execute. </summary>
        /// <returns> The <see cref="string"/>. </returns>
        public string Execute()
        {
            throw new NotImplementedException();
        }
    }
}
