﻿namespace Slack.Domain.Implementations.Services
{
    using System;
    using System.IO;
    using System.Net.Http;

    using Newtonsoft.Json;

    using Slack.Domain.Models;

    using WebSocket4Net;

    /// <summary>
    /// The slack gateway.
    /// </summary>
    public class SlackGateway
    {
        void websocket_DataReceived(object sender, DataReceivedEventArgs e)
        {
            var a = 0;
        }

        void websocket_Closed(object sender, EventArgs e)
        {
            var a = 0;
        }

        void websocket_Error(object sender, SuperSocket.ClientEngine.ErrorEventArgs e)
        {
            var a = 0;
        }

        void websocket_Opened(object sender, EventArgs e)
        {
            var a = 0;
        }

        void websocket_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            var a = 0;
        }

        /// <summary> The start connection. </summary>
        /// <param name="token"> The token. </param>
        /// <returns> The <see cref="LoginResponseMessage"/>. </returns>
        public LoginResponseMessage StartConnection(string token)
        {
            var uriBuilder = new UriBuilder("https://slack.com/api/rtm.start")
                                 {
                                     Query = string.Format("token={0}", token)
                                 };
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, uriBuilder.ToString());

            var httpClient = new HttpClient();
            var responseMessage = httpClient.SendAsync(httpRequestMessage).Result;
            responseMessage.EnsureSuccessStatusCode();

            if (!responseMessage.IsSuccessStatusCode)
            {
                return null;
            }
            string jsonMessage;
            using (var responseStream = responseMessage.Content.ReadAsStreamAsync().Result)
            {
                jsonMessage = new StreamReader(responseStream).ReadToEnd();
            }

            var tokenResponse = (LoginResponseMessage)JsonConvert.DeserializeObject(jsonMessage, typeof(LoginResponseMessage));
            return tokenResponse;
        }

        /// <summary> The start websocket. </summary>
        /// <param name="url"> The url. </param>
        public void StartWebsocket(string url)
        {
            var websocket = new WebSocket(url);
            websocket.Opened += this.websocket_Opened;
            websocket.Error += this.websocket_Error;
            websocket.Closed += this.websocket_Closed;
            websocket.DataReceived += this.websocket_DataReceived;
            websocket.MessageReceived += this.websocket_MessageReceived;
            websocket.Open();
        }
    }
}
