﻿namespace Slack.Domain.Implementations
{
    using System.Collections.Generic;

    using Slack.Domain.Contracts;
    using Slack.Domain.Contracts.Commands;
    using Slack.Domain.Implementations.Services;

    /// <summary> The base bot. </summary>
    public abstract class BaseBot : IBot
    {
        /// <summary> Initializes a new instance of the <see cref="BaseBot"/> class.  </summary>
        /// <param name="token"> The token.  </param>
        /// <param name="commands"> The commands. </param>
        /// <param name="messageCommands"> The message Commands. </param>
        protected BaseBot(string token, IEnumerable<ICommand> commands, IEnumerable<IMessageCommand> messageCommands)
        {
            this.Token = token;
            this.Commands = commands;
            this.MessageCommands = messageCommands;
        }

        /// <summary> Gets or sets the message commands. </summary>
        private IEnumerable<IMessageCommand> MessageCommands
        {
            get;
            set;
        }

        /// <summary> Gets or sets the commands. </summary>
        private IEnumerable<ICommand> Commands
        {
            get;
            set;
        }

        /// <summary> Gets or sets the token. </summary>
        private string Token
        {
            get;
            set;
        }

        /// <summary> The run. </summary>
        public void Run()
        {
            var slackGateway = new SlackGateway();
            var session = slackGateway.StartConnection(this.Token);
            slackGateway.StartWebsocket(session.Url);
        }
    }
}
